package com.neuralnoise.sparqlm.service.mine;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neuralnoise.sparqlm.model.data.DataSet;
import com.neuralnoise.sparqlm.model.task.RankTask;

@Service
public class RankServiceImpl implements RankService {

	private static final Logger log = LoggerFactory.getLogger(RankServiceImpl.class);
	
	@Autowired
	protected WekaService wekaService;
	
	public List<Pair<Map<String, Object>, Double>> rank(RankTask task, DataSet dataSet) throws Exception {
		final List<Pair<Map<String, Object>, Double>> rank = wekaService.logisticRegression(task, dataSet);
		return rank;
	}
	
}
