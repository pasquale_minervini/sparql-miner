package com.neuralnoise.sparqlm.service.mine;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neuralnoise.sparqlm.model.data.DataSet;
import com.neuralnoise.sparqlm.model.task.ClusterTask;

@Service
public class ClusterServiceImpl implements ClusterService {

	private static final Logger log = LoggerFactory.getLogger(ClusterServiceImpl.class);
	
	@Autowired
	protected WekaService wekaService;

	public List<Pair<Map<String, Object>, Integer>> cluster(ClusterTask task, DataSet dataSet) throws Exception {
		final List<Pair<Map<String, Object>, Integer>> clusters = wekaService.hardCluster(task, dataSet);
		return clusters;
	}
	
}
