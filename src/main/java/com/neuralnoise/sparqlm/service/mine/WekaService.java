package com.neuralnoise.sparqlm.service.mine;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.neuralnoise.sparqlm.model.data.DataSet;
import com.neuralnoise.sparqlm.model.task.ClusterTask;
import com.neuralnoise.sparqlm.model.task.RankTask;

/**
 * Service wrapping Weka's learning functionalities, interface.
 * 
 * @author pasquale
 */
public interface WekaService {

	public List<Pair<Map<String, Object>, Integer>> hardCluster(ClusterTask task, DataSet dataSet) throws Exception;
	
	public List<Pair<Map<String, Object>, Double>> logisticRegression(RankTask task, DataSet dataSet) throws Exception;

}
