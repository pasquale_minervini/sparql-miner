package com.neuralnoise.sparqlm.service.mine.util;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.neuralnoise.sparqlm.model.data.DataSet;
import com.neuralnoise.sparqlm.model.variable.Variable;
import com.neuralnoise.sparqlm.model.variable.ContinuousVariable;
import com.neuralnoise.sparqlm.model.variable.DiscreteVariable;

public class TypeUtils {

	private static final Logger log = LoggerFactory.getLogger(TypeUtils.class);
	
	private TypeUtils() { }
	
	/**
	 * Turn a list of string bindings into a list of (typed) variable bindings.
	 * 
	 * @param bindings		List of string bindings.
	 * @param task			Mining task (which might specifies the type of each variable).
	 * @return				List of variable bindings.
	 */
	public static DataSet toDataSet(List<Map<String, Object>> bindings) {
		final DataSet dataSet = new DataSet(bindings);
		return dataSet;
	}
	
	public static Map<String, Variable> inferTypes(List<Map<String, Object>> bindings) {
		Map<String, Variable> types = Maps.newTreeMap();
		final Set<String> varNames = Sets.newTreeSet();
		
		for (Map<String, Object> binding : bindings) {
			varNames.addAll(binding.keySet());
		}
		
		for (final String varName : varNames) {
			// Try to infer the type of varName
			Variable var = inferType(varName, bindings);
			types.put(varName, var);
		}
		
		return types;
	}

	public static Variable inferType(String varName, List<Map<String, Object>> bindings) {
		final Set<Object> varValues = Sets.newHashSet();
		for (Map<String, Object> binding : bindings) {
			if (binding.containsKey(varName)) {
				varValues.add(binding.get(varName));
			}
		}
		boolean isDiscrete = false;
		Iterator<Object> varValuesIt = varValues.iterator();
		while (varValuesIt.hasNext() && !isDiscrete) {
			Object varValue = varValuesIt.next();
			if (varValue != null) {
				if (varValue instanceof String) {
					isDiscrete = true;
				}
			}
		}
		Variable var = null;
		if (isDiscrete) {
			final Set<String> varValuesStr = Sets.newTreeSet();
			for (Object o : varValues) {
				varValuesStr.add(o.toString());
			}
			var = new DiscreteVariable(varValuesStr);
		} else {
			var = new ContinuousVariable();
		}
		return var;
	}
	
}
