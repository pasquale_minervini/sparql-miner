package com.neuralnoise.sparqlm.service.mine;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.neuralnoise.sparqlm.model.data.DataSet;
import com.neuralnoise.sparqlm.model.task.ClusterTask;

public interface ClusterService {

	public List<Pair<Map<String, Object>, Integer>> cluster(ClusterTask task, DataSet dataSet) throws Exception;
	
}
