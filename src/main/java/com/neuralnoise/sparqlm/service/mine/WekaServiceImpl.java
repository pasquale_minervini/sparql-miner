package com.neuralnoise.sparqlm.service.mine;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import weka.classifiers.functions.Logistic;
import weka.clusterers.Clusterer;
import weka.core.Instance;
import weka.core.Instances;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.neuralnoise.sparqlm.model.data.DataSet;
import com.neuralnoise.sparqlm.model.task.ClusterTask;
import com.neuralnoise.sparqlm.model.task.RankTask;
import com.neuralnoise.sparqlm.service.mine.util.WekaUtils;

/**
 * Service wrapping Weka's learning functionalities, implementation.
 * 
 * @author pasquale
 */
@Service
public class WekaServiceImpl implements WekaService {
	
	private static final Logger log = LoggerFactory.getLogger(WekaServiceImpl.class);
	
	/**
	 * Given a clustering task and a list of variable bindings (each binding corresponds to a single instance),
	 * returns a hard clustering for each of the provided instances.
	 * 
	 * @param task		Clustering task.
	 * @param bindingList	List of variable bindings.
	 * @return			Map with a cluster identifier for each binding.
	 */
	public List<Pair<Map<String, Object>, Integer>> hardCluster(ClusterTask task, DataSet dataSet) throws Exception {

		Map<Instance, Map<String, Object>> mapping = Maps.newHashMap();
		Instances data = WekaUtils.makeInstances(task, dataSet, mapping);
	
		Clusterer clusterer = WekaUtils.makeClusterer(task, data);
		List<Pair<Map<String, Object>, Integer>> distribution = Lists.newLinkedList();
		
		for (Entry<Instance, Map<String, Object>> entry : mapping.entrySet()) {
			Instance instance = entry.getKey();
			Map<String, Object> binding = entry.getValue();
			
			final int clusterId = clusterer.clusterInstance(instance);	
		
			Pair<Map<String, Object>, Integer> pair = Pair.of(binding, clusterId);
			distribution.add(pair);
		}
		
		return distribution;
	}

	public List<Pair<Map<String, Object>, Double>> logisticRegression(RankTask task, DataSet dataSet) throws Exception {
		
		Map<Instance, Map<String, Object>> mapping = Maps.newHashMap();
		Instances instances = WekaUtils.makeInstances(task, dataSet, mapping);

		System.out.println(instances);
		
		Logistic logistic = WekaUtils.makeLogistic(task, instances);
		
		List<Pair<Map<String, Object>, Double>> distribution = Lists.newLinkedList();
		
		for (Entry<Instance, Map<String, Object>> entry : mapping.entrySet()) {
			Instance instance = entry.getKey();
			Map<String, Object> binding = entry.getValue();
			
			double[] distributionForInstance = logistic.distributionForInstance(instance);
			final double probability = distributionForInstance[0];
			
			Pair<Map<String, Object>, Double> pair = Pair.of(binding, probability);
			distribution.add(pair);
		}
		
		return distribution;
	}
	
}
