package com.neuralnoise.sparqlm.service.mine.util;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import weka.classifiers.functions.Logistic;
import weka.clusterers.Clusterer;
import weka.clusterers.SimpleKMeans;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.neuralnoise.sparqlm.model.data.DataSet;
import com.neuralnoise.sparqlm.model.task.AbstractMiningTask;
import com.neuralnoise.sparqlm.model.task.ClusterTask;
import com.neuralnoise.sparqlm.model.task.RankTask;
import com.neuralnoise.sparqlm.model.variable.Variable;
import com.neuralnoise.sparqlm.model.variable.DiscreteVariable;

public class WekaUtils {

	private static final Logger log = LoggerFactory.getLogger(WekaUtils.class);
	
	public static final String RELATION_NAME = "relation";
	
	private WekaUtils() { }
	
	public static Clusterer makeClusterer(ClusterTask task, Instances instances) throws Exception {
		//System.out.println("Instances: " + instances);
		
		Clusterer clusterer = new SimpleKMeans(); //new HierarchicalClusterer();
		clusterer.buildClusterer(instances);
		return clusterer;
	}
	
	public static Logistic makeLogistic(RankTask task, Instances data) throws Exception {
		Logistic logistic = new Logistic();
		logistic.buildClassifier(data);
		return logistic;
	}
	
	public static Instances makeInstances(AbstractMiningTask task, DataSet dataSet, Map<Instance, Map<String, Object>> mapping) {
		Map<String, Variable> varTypes = task.getVariables();
			
		if (varTypes == null) {
			varTypes = TypeUtils.inferTypes(dataSet.getValues());
		} else {
			Map<String, Variable> _varTypes = Maps.newHashMap();
			for (Entry<String, Variable> entry : varTypes.entrySet()) {
				String key = entry.getKey();
				Variable value = entry.getValue();
				
				if (value == null || value.getClass() == Variable.class) {
					value = TypeUtils.inferType(key, dataSet.getValues());
				}
				
				_varTypes.put(key, value);
			}
			varTypes = _varTypes;
		}
		
		final int nVars = varTypes.size();
		final int nInstances = dataSet.getValues().size();
		
		final FastVector attributes = new FastVector(nVars);
		final Map<Variable, Attribute> varAttributeMap = Maps.newHashMap();
		
		final String classVariable = task.getClassVariable();
		
		int c = 0, classIndex = -1;
		
		final Set<Attribute> allAttributes = Sets.newHashSet();
		
		for (Entry<String, Variable> varEntry : varTypes.entrySet()) {
			final String varName = varEntry.getKey();
			Variable var = varEntry.getValue();
			final boolean isDiscrete = (var instanceof DiscreteVariable);
			
			Attribute attribute = null;
			if (isDiscrete) {
				Set<Object> varValues = WekaUtils.getVariableValues(varName, dataSet);
				FastVector vals = new FastVector(varValues.size());
				for (Object varValue : varValues) {
					vals.addElement(varValue);
				}
				attribute = new Attribute(varName, vals);
			} else {
				attribute = new Attribute(varName);
			}

			varAttributeMap.put(var, attribute);
			attributes.addElement(attribute);
			
			if (varName.equals(classVariable)) {
				classIndex = c;
			}
			c++;
			
			allAttributes.add(attribute);
		}
			
		Instances instances = new Instances(RELATION_NAME, attributes, nInstances);
		instances.setClassIndex(classIndex);
		
		for (Map<String, Object> binding : dataSet.getValues()) {
			Instance instance = new Instance(allAttributes.size());
			
			Set<Attribute> setAttributes = Sets.newHashSet();
			
			for (Entry<String, Object> entry : binding.entrySet()) {
				String varName = entry.getKey();
				Variable var = varTypes.get(varName); //dataSet.getType(varName);
				
				Object value = entry.getValue();
				
				Attribute attribute = varAttributeMap.get(var);
				
				if (value instanceof Double) {
					final double dvalue = (Double) value;
					instance.setValue(attribute, dvalue);
				} else {
					final String svalue = value.toString();
					instance.setValue(attribute, svalue);
				}
				
				setAttributes.add(attribute);
			}
			
			Set<Attribute> unsetAttributes = Sets.difference(allAttributes, setAttributes);
			for (Attribute unsetAttribute : unsetAttributes) {
				instance.setMissing(unsetAttribute);
			}
			
			instance.setDataset(instances);
			instances.add(instance);
			
			if (mapping != null) {
				mapping.put(instance, binding);
			}
		}
		
		return instances;
	}
	
	protected static Set<Object> getVariableValues(String varName, DataSet dataSet) {
		Set<Object> set = Sets.newHashSet();
		for (Map<String, Object> instance : dataSet.getValues()) {
			if (instance.containsKey(varName)) {
				Object value = instance.get(varName);
				set.add(value);
			}
		}
		return set;
	}
		
}
