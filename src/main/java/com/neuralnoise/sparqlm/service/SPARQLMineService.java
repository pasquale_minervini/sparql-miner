package com.neuralnoise.sparqlm.service;

import java.util.List;

import com.neuralnoise.sparqlm.model.data.ClusteredQueryResult;
import com.neuralnoise.sparqlm.model.request.QueryClusterRequest;

/**
 *  Service for querying endpoints, and automatically executing mining tasks on results, interface.
 * 
 * @author pasquale
 */
public interface SPARQLMineService {

	public List<ClusteredQueryResult> sparql(QueryClusterRequest qcr) throws Exception;
	
}
