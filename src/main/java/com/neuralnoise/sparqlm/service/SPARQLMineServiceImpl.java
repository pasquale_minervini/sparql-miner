package com.neuralnoise.sparqlm.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.neuralnoise.sparqlm.model.data.ClusteredQueryResult;
import com.neuralnoise.sparqlm.model.data.DataSet;
import com.neuralnoise.sparqlm.model.request.ClusterRequest;
import com.neuralnoise.sparqlm.model.request.QueryClusterRequest;
import com.neuralnoise.sparqlm.service.mine.ClusterService;
import com.neuralnoise.sparqlm.service.mine.RankService;
import com.neuralnoise.sparqlm.service.query.QueryService;

/**
 * Service for querying endpoints, and automatically executing mining tasks on results, implementation.
 * 
 * @author pasquale
 */
@Service
public class SPARQLMineServiceImpl implements SPARQLMineService {

	private static final Logger log = LoggerFactory.getLogger(SPARQLMineServiceImpl.class);
	
	@Autowired
	protected QueryService queryService;
	
	@Autowired
	protected RankService rankingService;
	
	@Autowired
	protected ClusterService clusteringService;
	
	/**
	 * Execute a SPARQL query against a endpoint, and cluster results (each output binding corresponds to an instance).
	 * 
	 * @param endpoint		SPARQL endpoint.
	 * @param sparqlQuery	SPARQL query.
	 * @param task			Clustering task.
	 * @return 				A cluster assignment for each output binding.
	 */
	public List<ClusteredQueryResult> sparql(QueryClusterRequest qcr) throws Exception {
		List<Map<String, Object>> queryResults = this.queryService.sparql(qcr.getEndpoint(), qcr.getQuery());
		final DataSet dataSet = new DataSet(queryResults);
		ClusterRequest clusterRequest = new ClusterRequest(qcr.getClusterTask(), dataSet);
		List<Pair<Map<String, Object>, Integer>> _clusteredQueryResults = this.clusteringService.cluster(clusterRequest.getTask(), clusterRequest.getDataSet());
		List<ClusteredQueryResult> results = Lists.newLinkedList();
		for (Pair<Map<String, Object>, Integer> _clusteredQueryResult : _clusteredQueryResults) {
			final Map<String, Object> queryResult = _clusteredQueryResult.getKey();
			final int clusterId = _clusteredQueryResult.getValue();
			
			ClusteredQueryResult cqr = new ClusteredQueryResult(queryResult, clusterId);
			results.add(cqr);
		}
		return results;
	}
	
}
