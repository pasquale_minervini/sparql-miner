package com.neuralnoise.sparqlm.service.query;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.sparql.util.FmtUtils;
import com.hp.hpl.jena.vocabulary.XSD;

/**
 * Generic service for querying endpoints, implementation.
 * 
 * @author pasquale
 */
@Service
public class QueryServiceImpl implements QueryService {

	private static final Logger log = LoggerFactory.getLogger(QueryServiceImpl.class);
	
	@Autowired
	protected SPARQLService sparqlService;
	
	private static Object value(RDFNode node) {
		Object value = null;
		if (node.isLiteral()) {
			final Literal literal = node.asLiteral();
			final String datatype = literal.getDatatypeURI(), s = literal.getLexicalForm();
			
			if (datatype != null) {
				Double dval = null;
				if (datatype.equals(XSD.integer.getURI())
						|| datatype.equals(XSD.decimal.getURI())
						|| datatype.equals(XSD.xdouble.getURI())) {
					dval = Double.parseDouble(s);
					value = dval;
				}
			}
		}
		if (value == null) {
			value = FmtUtils.stringForRDFNode(node);
		}
		return value;
	}
	
	public List<Map<String, Object>> sparql(final String endpoint, final String sparqlQuery) {
		final List<Map<String, RDFNode>> results = sparqlService.query(endpoint, sparqlQuery);
		final List<Map<String, Object>> ret = Lists.newLinkedList();

		for (Map<String, RDFNode> result : results) {
			final Map<String, Object> qs = Maps.newTreeMap();
			for (Entry<String, RDFNode> entry : result.entrySet()) {
				final String key = entry.getKey();
				final Object value = value(entry.getValue());		
				qs.put(key, value);
			}
			ret.add(qs);
		}
		
		return ret;
	}
	
}
