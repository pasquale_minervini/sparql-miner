package com.neuralnoise.sparqlm.service.query;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;

/**
 * Service for querying SPARQL endpoints, implementation.
 * 
 * @author pasquale
 */
@Service
public class SPARQLServiceImpl implements SPARQLService {

	private static final Logger log = LoggerFactory.getLogger(SPARQLServiceImpl.class);
	
	/**
	 * Runs a SPARQL query against a SPARQL endpoint, and returns a list of variable bindings.
	 * 
	 * @param endpoint		SPARQL endpoint.
	 * @param sparqlQuery	SPARQL query.
	 */
	public List<Map<String, RDFNode>> query(final String endpoint, final String sparqlQuery) {
		Query query = QueryFactory.create(sparqlQuery);
		
		QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint, query);
		ResultSet results = qexec.execSelect();

		List<Map<String, RDFNode>> ret = Lists.newLinkedList();

		while (results.hasNext()) {
			QuerySolution qs = results.next();
			Map<String, RDFNode> row = Maps.newTreeMap();
			Iterator<String> varNames = qs.varNames();
			
			while (varNames.hasNext()) {
				final String varName = varNames.next();
				RDFNode value = qs.get(varName);
				row.put(varName, value);
			}
			
			ret.add(row);
		}
		
		qexec.close();
		return ret;
	}
	
}
