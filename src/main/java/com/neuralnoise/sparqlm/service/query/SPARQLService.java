package com.neuralnoise.sparqlm.service.query;

import java.util.List;
import java.util.Map;

import com.hp.hpl.jena.rdf.model.RDFNode;

/**
 * Service for querying SPARQL endpoints, interface.
 * 
 * @author pasquale
 */
public interface SPARQLService {

	public List<Map<String, RDFNode>> query(final String endpoint, final String sparqlQuery);
	
}
