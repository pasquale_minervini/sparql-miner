package com.neuralnoise.sparqlm.service.query;

import java.util.List;
import java.util.Map;

/**
 * Generic service for querying endpoints, interface.
 * 
 * @author pasquale
 */
public interface QueryService {

	public List<Map<String, Object>> sparql(final String endpoint, final String sparqlQuery);
	
}
