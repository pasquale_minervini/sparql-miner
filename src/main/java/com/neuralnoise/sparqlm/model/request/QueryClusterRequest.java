package com.neuralnoise.sparqlm.model.request;

import com.neuralnoise.sparqlm.model.task.ClusterTask;

public class QueryClusterRequest {

	private String query;
	private String endpoint;
	private ClusterTask clusterTask;
	
	public QueryClusterRequest(String query, String endpoint, ClusterTask clusterTask) {
		this.setQuery(query);
		this.setEndpoint(endpoint);
		this.setClusterTask(clusterTask);
	}
	
	public QueryClusterRequest() { }

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public ClusterTask getClusterTask() {
		return clusterTask;
	}

	public void setClusterTask(ClusterTask clusterTask) {
		this.clusterTask = clusterTask;
	}
	
}
