package com.neuralnoise.sparqlm.model.request;

import com.neuralnoise.sparqlm.model.data.DataSet;
import com.neuralnoise.sparqlm.model.task.ClusterTask;

public class ClusterRequest {

	private ClusterTask task;
	private DataSet dataSet;
	
	public ClusterRequest(ClusterTask task, DataSet dataSet) {
		setTask(task);
		setDataSet(dataSet);
	}
	
	public ClusterRequest() { }

	public ClusterTask getTask() {
		return task;
	}

	public void setTask(ClusterTask task) {
		this.task = task;
	}

	public DataSet getDataSet() {
		return dataSet;
	}

	public void setDataSet(DataSet dataSet) {
		this.dataSet = dataSet;
	}
	
}
