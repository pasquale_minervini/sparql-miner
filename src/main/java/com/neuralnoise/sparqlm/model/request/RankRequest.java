package com.neuralnoise.sparqlm.model.request;

import com.neuralnoise.sparqlm.model.data.DataSet;
import com.neuralnoise.sparqlm.model.task.RankTask;

public class RankRequest {

	private RankTask task;
	private DataSet dataSet;
	
	public RankRequest() { }

	public RankTask getTask() {
		return task;
	}

	public void setTask(RankTask task) {
		this.task = task;
	}

	public DataSet getDataSet() {
		return dataSet;
	}

	public void setDataSet(DataSet dataSet) {
		this.dataSet = dataSet;
	}
	
}
