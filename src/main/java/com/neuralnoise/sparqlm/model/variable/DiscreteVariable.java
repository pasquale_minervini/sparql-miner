package com.neuralnoise.sparqlm.model.variable;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DiscreteVariable extends Variable {

	private static final long serialVersionUID = -3947881219457590021L;

	@JsonProperty
	protected final Set<String> values;

	public DiscreteVariable(Set<String> values) {
		super();
		this.values = values;
	}
	
	public Set<String> getValues() {
		return values;
	}

	@Override
	public String toString() {
		return "DiscreteVariable [values=" + values + "]";
	}

}
