package com.neuralnoise.sparqlm.model.variable;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
		use = JsonTypeInfo.Id.NAME,
		include = JsonTypeInfo.As.PROPERTY,
		property = "type", visible = true)
@JsonSubTypes({
	@Type(value = Variable.class, name = "variable"),
	@Type(value = ContinuousVariable.class, name = "continuous"),
	@Type(value = DiscreteVariable.class, name = "discrete")
})
public class Variable implements Serializable {
	
	private static final long serialVersionUID = 9047322389771226645L;
	
	private static final Logger log = LoggerFactory.getLogger(Variable.class);
	
	public Variable() { }
	
}
