package com.neuralnoise.sparqlm.model.variable;


public class ContinuousVariable extends Variable {

	private static final long serialVersionUID = -227639619425073736L;
	
	public ContinuousVariable() {
		super();
	}

	@Override
	public String toString() {
		return "ContinuousVariable []";
	}
	
}
