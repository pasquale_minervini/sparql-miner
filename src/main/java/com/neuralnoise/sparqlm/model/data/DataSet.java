package com.neuralnoise.sparqlm.model.data;

import java.util.List;
import java.util.Map;

public class DataSet {

	private List<Map<String, Object>> values;
	
	public DataSet(List<Map<String, Object>> values) {
		this.values = values;
	}
	
	public DataSet() { }

	public List<Map<String, Object>> getValues() {
		return values;
	}
	
	public void setValues(List<Map<String, Object>> values) {
		this.values = values;
	}
	
}
