package com.neuralnoise.sparqlm.model.data;

import java.util.Map;

public class ClusteredQueryResult {

	private Map<String, Object> queryResult;
	private int clusterId;
	
	public ClusteredQueryResult(Map<String, Object> queryResult, int clusterId) {
		this.queryResult = queryResult;
		this.clusterId = clusterId;
	}
	
	public ClusteredQueryResult() { }

	public Map<String, Object> getQueryResult() {
		return queryResult;
	}
	
	public void setQueryResult(Map<String, Object> queryResult) {
		this.queryResult = queryResult;
	}

	public int getClusterId() {
		return clusterId;
	}

	public void setClusterId(int clusterId) {
		this.clusterId = clusterId;
	}
	
	@Override
	public String toString() {
		return "ClusteredQueryResult [queryResult=" + queryResult + ", clusterId=" + clusterId + "]";
	}
}
