package com.neuralnoise.sparqlm.model.task;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.neuralnoise.sparqlm.model.variable.Variable;

public class RankTask extends AbstractMiningTask {

	private static final Logger log = LoggerFactory.getLogger(RankTask.class);
	
	@JsonProperty
	private String classVariable;
	
	public RankTask(Map<String, Variable> variables, String classVariable) {
		super(variables);
		this.classVariable = classVariable;
	}
	
	public RankTask(String classVariable) {
		super();
		this.classVariable = classVariable;
	}
	
	public RankTask() {
		
	}

	@Override
	public String getClassVariable() {
		return classVariable;
	}
	
	public void setClassVariable(String classVariable) {
		this.classVariable = classVariable;
	}

	@Override
	public String toString() {
		return "RankingTask [classVariable=" + classVariable + ", variables=" + variables + "]";
	}

}
