package com.neuralnoise.sparqlm.model.task;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralnoise.sparqlm.model.variable.Variable;

public class ClusterTask extends AbstractMiningTask {
	
	private static final Logger log = LoggerFactory.getLogger(ClusterTask.class);
	
	public ClusterTask(Map<String, Variable> variables) {
		super(variables);
	}
	
	public ClusterTask() {
		super();
	}

	@Override
	public String toString() {
		return "ClusteringTask [variables=" + variables + "]";
	}

}
