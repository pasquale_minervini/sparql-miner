package com.neuralnoise.sparqlm.model.task;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.neuralnoise.sparqlm.model.variable.Variable;

@JsonTypeInfo(
		use = JsonTypeInfo.Id.NAME,
		include = JsonTypeInfo.As.PROPERTY,
		property = "type", visible = true)
@JsonSubTypes({
	@Type(value = ClusterTask.class, name = "clustering"),
	@Type(value = RankTask.class, name = "ranking")
})
public abstract class AbstractMiningTask {

	private static final Logger log = LoggerFactory.getLogger(AbstractMiningTask.class);
	
	protected Map<String, Variable> variables;

	public AbstractMiningTask(Map<String, Variable> variables) {
		setVariables(variables);
	}
	
	public AbstractMiningTask() {
		
	}
	
	public Map<String, Variable> getVariables() {
		return variables;
	}
	
	public void setVariables(Map<String, Variable> variables) {
		this.variables = variables;
	}

	@JsonIgnore
	public String getClassVariable() {
		return null;
	}
	
}
