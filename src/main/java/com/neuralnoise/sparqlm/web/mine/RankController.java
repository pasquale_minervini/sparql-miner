package com.neuralnoise.sparqlm.web.mine;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.neuralnoise.sparqlm.model.data.Obj;
import com.neuralnoise.sparqlm.model.request.RankRequest;

@Controller
@RequestMapping("/v1/rank")
public class RankController extends AbstractMineController {

	private static final Logger log = LoggerFactory.getLogger(RankController.class);
	
	@RequestMapping(value = { "/rank" }, method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public List<Pair<Map<String, Object>, Double>> rank(
			@RequestBody RankRequest rankRequest,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<Pair<Map<String, Object>, Double>> results = rankService.rank(rankRequest.getTask(), rankRequest.getDataSet());
		response.setStatus(HttpStatus.OK.value());	
		return results;
	}
	
	@RequestMapping(value= { "/test" }, method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Obj test(HttpServletRequest request, HttpServletResponse response) {
		return new Obj("hello", "world");
	}
	
}
