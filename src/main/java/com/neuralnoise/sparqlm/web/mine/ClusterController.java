package com.neuralnoise.sparqlm.web.mine;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Sets;
import com.neuralnoise.sparqlm.model.request.ClusterRequest;
import com.neuralnoise.sparqlm.model.task.ClusterTask;
import com.neuralnoise.sparqlm.model.variable.Variable;
import com.neuralnoise.sparqlm.model.variable.DiscreteVariable;

@Controller
@RequestMapping("/v1/cluster")
public class ClusterController extends AbstractMineController {

	private static final Logger log = LoggerFactory.getLogger(ClusterController.class);
	
	@RequestMapping(value = { "/cluster" }, method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public List<Pair<Map<String, Object>, Integer>> cluster(
			@RequestBody ClusterRequest clusterRequest,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<Pair<Map<String, Object>, Integer>> results = clusterService.cluster(clusterRequest.getTask(), clusterRequest.getDataSet());
		response.setStatus(HttpStatus.OK.value());	
		return results;
	}

	@RequestMapping(value = { "/test" }, method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Variable test(HttpServletRequest request, HttpServletResponse response) {
		response.setStatus(HttpStatus.OK.value());
		return new DiscreteVariable(Sets.newHashSet("true", "false"));
	}

	//  curl -i -H "Content-Type: application/json" -X POST -d '{"type":"clustering","variables":{"a":{"type":"continuous"}}}' http://127.0.0.1:9966/sm/v1/cluster/task
	@RequestMapping(value = { "/task" }, method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String task(@RequestBody ClusterTask task,
			HttpServletRequest request, HttpServletResponse response) {
		System.out.println(task);
		response.setStatus(HttpStatus.OK.value());
		return task.toString();
	}
	
}
