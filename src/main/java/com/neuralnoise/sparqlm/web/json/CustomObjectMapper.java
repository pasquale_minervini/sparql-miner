package com.neuralnoise.sparqlm.web.json;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class CustomObjectMapper extends ObjectMapper {

	private static final long serialVersionUID = -5723512101734483782L;

	private static final Logger log = LoggerFactory.getLogger(CustomObjectMapper.class);
	
	public CustomObjectMapper() {
		super();

		SimpleModule module = new SimpleModule("SMModule");
		
		this.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		this.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
		//this.enableDefaultTypingAsProperty(DefaultTyping.JAVA_LANG_OBJECT, JsonTypeInfo.Id.CLASS.getDefaultPropertyName());
		
		this.registerModule(module);
	}
	
}
