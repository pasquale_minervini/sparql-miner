package com.neuralnoise.sparqlm.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.neuralnoise.sparqlm.model.data.ClusteredQueryResult;
import com.neuralnoise.sparqlm.model.request.QueryClusterRequest;

@Controller
@RequestMapping("/v1/sm")
public class SPARQLMineController extends AbstractSPARQLMineController {

	@RequestMapping(value = { "/cluster" }, method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<ClusteredQueryResult> sparql(@RequestBody QueryClusterRequest qcr,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<ClusteredQueryResult> results = this.sparqlMineService.sparql(qcr);
		response.setStatus(HttpStatus.OK.value());
		return results;
	}
	
}
