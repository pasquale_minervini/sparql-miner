package com.neuralnoise.sparqlm.web.query;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/v1/query")
public class QueryController extends AbstractQueryController {
	
	@RequestMapping(value = { "/sparql" }, method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Map<String, Object>> sparql(
			@RequestParam(value = "query", required = false) String query, @RequestParam(value = "endpoint", required = false) String endpoint,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		if (endpoint == null) {
			endpoint = "http://127.0.0.1:8890/sparql";
		}
		
		if (query == null) {
			query = "SELECT ?p ?o WHERE { <http://dbpedia.org/resource/Barack_Obama> ?p ?o . } LIMIT 100";
		}
		
		List<Map<String, Object>> results = this.queryService.sparql(endpoint, query);
		
		response.setStatus(HttpStatus.OK.value());
		
		return results;
	}
	
}
