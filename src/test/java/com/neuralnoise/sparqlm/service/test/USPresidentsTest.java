package com.neuralnoise.sparqlm.service.test;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.neuralnoise.sparqlm.model.data.ClusteredQueryResult;
import com.neuralnoise.sparqlm.model.data.DataSet;
import com.neuralnoise.sparqlm.model.request.QueryClusterRequest;
import com.neuralnoise.sparqlm.model.task.ClusterTask;
import com.neuralnoise.sparqlm.model.task.RankTask;
import com.neuralnoise.sparqlm.service.SPARQLMineService;
import com.neuralnoise.sparqlm.service.mine.RankService;
import com.neuralnoise.sparqlm.service.query.QueryService;
import com.neuralnoise.sparqlm.web.SPARQLMineController;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/META-INF/spring/web-context.xml")
public class USPresidentsTest {

	@Autowired
	public SPARQLMineService sparqlMineService;
	
	@Autowired
	public QueryService queryService;
	
	@Autowired
	public RankService rankService;
	
	private static final String NL = System.getProperty("line.separator");
	
	public static final String LOCALHOST = "http://127.0.0.1:8890/sparql";
	public static final String DBPEDIA = "http://dbpedia.org/sparql";
	
	public static final String QUERY_1 =
			"PREFIX dbpedia: <http://dbpedia.org/ontology/>" + NL +
			"PREFIX dbprop: <http://dbpedia.org/property/>" + NL +
			"PREFIX yago: <http://dbpedia.org/class/yago/>" + NL +
			NL +
			"SELECT DISTINCT ?president ?birthPlace ?party WHERE {" + NL +
			"   ?president a yago:PresidentsOfTheUnitedStates ." + NL +
			"   ?president dbpedia:birthPlace ?birthPlace ." + NL +
			"   ?president dbprop:party ?party ." + NL +
			//"   ?birthPlace a dbpedia:Region ." + NL +
			"   ?birthPlace a yago:StatesOfTheUnitedStates ." + NL +
			"}";

	//@Test
	public void query1Test() throws Exception {
		QueryClusterRequest qcr = new QueryClusterRequest();

		qcr.setClusterTask(new ClusterTask());
		qcr.setQuery(QUERY_1);
		qcr.setEndpoint(DBPEDIA);
		
		List<ClusteredQueryResult> results = sparqlMineService.sparql(qcr);
		
		for (ClusteredQueryResult result : results) {
			System.out.println("Result: " + result);
		}
	}
	
	public static class Result implements Comparable<Result> {

		public final Map<String, Object> bindings;
		public final double score;
		
		public Result(Map<String, Object> bindings, double score) {
			this.bindings = bindings;
			this.score = score;
		}
		
		@Override
		public int compareTo(Result arg) {
			return Double.compare(score, arg.score);
		}
		
		@Override
		public String toString() {
			return "Result [bindings=" + bindings + ", score=" + score + "]";
		}
		
	}
	
	@Test
	public void rank1Test() throws Exception {
		QueryClusterRequest qcr = new QueryClusterRequest();

		qcr.setClusterTask(new ClusterTask());
		qcr.setQuery(QUERY_1);
		qcr.setEndpoint(DBPEDIA);
		
		final List<Map<String, Object>> results = queryService.sparql(DBPEDIA, QUERY_1);

		Map<String, String> samples = Maps.newTreeMap();
		samples.put("<http://dbpedia.org/resource/Franklin_D._Roosevelt>", "1");
		samples.put("<http://dbpedia.org/resource/Martin_Van_Buren>", "1");
		samples.put("<http://dbpedia.org/resource/James_A._Garfield>", "0");
		samples.put("<http://dbpedia.org/resource/Lyndon_B._Johnson>", "0");
		
		for (Map<String, Object> result : results) {
			final String president = (String) result.get("president");
			if (samples.containsKey(president)) {
				result.put("relevant", samples.get(president));
			}
			System.out.println("Result: " + result);
		}
		
		RankTask task = new RankTask("relevant");
		DataSet dataSet = new DataSet(results);
		
		List<Pair<Map<String, Object>, Double>> rankedResults = rankService.rank(task, dataSet);
		
		List<Result> _results = Lists.newLinkedList();
		
		for (Pair<Map<String, Object>, Double> rankedResult : rankedResults) {
			_results.add(new Result(rankedResult.getKey(), rankedResult.getValue()));
		}
		
		//Collections.sort(_results, Collections.reverseOrder());
		Collections.sort(_results);
		
		for (Result _result : _results) {
			System.out.println(_result);
		}
		
	}
	
}
