package com.neuralnoise.sparqlm.service.mine.test;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.neuralnoise.sparqlm.model.data.DataSet;
import com.neuralnoise.sparqlm.model.task.ClusterTask;
import com.neuralnoise.sparqlm.service.mine.ClusterService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/META-INF/spring/web-context.xml")
public class ClusterTest {

	private static final Logger log = LoggerFactory.getLogger(ClusterTest.class);
	
	@Autowired
	private ClusterService clusterService;
	
	@Test
	public void clusterTest() throws Exception {
		
		ClusterTask task = new ClusterTask(null);

		List<Map<String, Object>> tuples = Lists.newLinkedList();
		
		tuples.add(binding("a", "a", "a"));
		tuples.add(binding("a", "a", "a"));
		tuples.add(binding("a", "a", "c"));
		
		tuples.add(binding("b", "b", "b"));
		tuples.add(binding("b", "b", "b"));
		tuples.add(binding("b", "b", "c"));
		
		DataSet dataSet = new DataSet(tuples);
		
		List<Pair<Map<String, Object>, Integer>> results = clusterService.cluster(task, dataSet);
		
		for (Pair<Map<String, Object>, Integer> result : results) {
			System.out.println(result);
		}
	}
	
	private static Map<String, Object> binding(Object a, Object b, Object c) {
		Map<String, Object> binding = Maps.newHashMap();
		binding.put("a", a);
		binding.put("b", b);
		binding.put("c", c);
		return binding;
	}
	
}
