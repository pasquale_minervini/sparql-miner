package com.neuralnoise.sparqlm.service.mine.test;

import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class WekaUtils {

	private static final Logger log = LoggerFactory.getLogger(WekaUtils.class);
	
	private WekaUtils() { }
	
	public static final Instances iris() throws Exception {
		URL res = Thread.currentThread().getContextClassLoader().getResource("datasets/iris.arff");
		DataSource source = new DataSource(res.getPath());
		Instances data = source.getDataSet();
		return data;
	}
	
}
