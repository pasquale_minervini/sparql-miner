package com.neuralnoise.sparqlm.service.mine.test;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import weka.classifiers.functions.Logistic;
import weka.clusterers.HierarchicalClusterer;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/META-INF/spring/web-context.xml")
public class WekaTest {

	private static final Logger log = LoggerFactory.getLogger(WekaTest.class);
	
	@Test
	public void attributesTest() {
		Attribute cattr = new Attribute("cattr");
		
		FastVector vals = new FastVector(2);
		vals.addElement("head");
		vals.addElement("tail");
		
		Attribute dattr = new Attribute("dattr", vals);

		FastVector attributes = new FastVector(2);
		attributes.addElement(cattr);
		attributes.addElement(dattr);
		
		Instances data = new Instances("relation", attributes, 1);
		
		Instance inst = new Instance(2);
		inst.setDataset(data);
		
		inst.setValue(cattr, 1.0);
		inst.setValue(dattr, "tail");

		data.add(inst);
		
		log.info("Data: " + data);
	}
	
	@Test
	public void hierachicalClusteringTest() throws Exception {
		Instances data = WekaUtils.iris();
		HierarchicalClusterer scheme = new HierarchicalClusterer();
		
		scheme.buildClusterer(data);
		scheme.setPrintNewick(true);
		
		log.info("Scheme: " + scheme);
	}
	
	@Test
	public void logisticRegressionTest() throws Exception {
		Instances iris = WekaUtils.iris();
		System.out.println("Iris: " + iris);
		if (iris.classIndex() == -1) {
			iris.setClassIndex(iris.numAttributes() - 1);
		}
		System.out.println("Class Index: " + iris.classIndex());
		Logistic lr = new Logistic();
		lr.buildClassifier(iris);
		System.out.println("LR: " + lr);
		for (int i = 0; i < iris.numInstances(); ++i) {
			Instance instance = iris.instance(i);
			System.out.println("LL:" + lr.classifyInstance(instance));
			System.out.println("Distribution: " + Arrays.toString(lr.distributionForInstance(instance)));
		}
	}
	
}
