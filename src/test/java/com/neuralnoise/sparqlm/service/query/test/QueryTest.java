package com.neuralnoise.sparqlm.service.query.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.neuralnoise.sparqlm.service.query.QueryService;
import com.neuralnoise.sparqlm.service.query.QueryServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/META-INF/spring/web-context.xml")
public class QueryTest {

	private static final Logger log = LoggerFactory.getLogger(QueryTest.class);
	
	@Test
	public void queryTest() {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("META-INF/spring/servlet-context.xml");
		QueryService queryService = (QueryService) ctx.getBean(QueryServiceImpl.class);

		final String endpoint = "http://127.0.0.1:8890/sparql";
		final String query = "SELECT ?p ?o WHERE { <http://dbpedia.org/resource/Barack_Obama> ?p ?o . } LIMIT 100";
		
		//queryService.sparql(endpoint, query);
	}

}
