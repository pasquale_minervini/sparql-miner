package com.neuralnoise.sparqlm.web.json.test;

import java.util.Map;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;
import com.neuralnoise.sparqlm.model.task.AbstractMiningTask;
import com.neuralnoise.sparqlm.model.task.ClusterTask;
import com.neuralnoise.sparqlm.model.task.RankTask;
import com.neuralnoise.sparqlm.model.variable.Variable;
import com.neuralnoise.sparqlm.model.variable.ContinuousVariable;
import com.neuralnoise.sparqlm.web.json.CustomObjectMapper;

public class CustomObjectMapperTest {

	private static final Logger log = LoggerFactory.getLogger(CustomObjectMapperTest.class);
	
	@Test
	public void clusterTest() throws Exception {
		CustomObjectMapper com = new CustomObjectMapper();
		
		Map<String, Variable> types = Maps.newHashMap();
		types.put("a", new ContinuousVariable());
		
		AbstractMiningTask task = new ClusterTask(types);
		
		final String json = com.writeValueAsString(task);
		
		log.info("JSON: " + json);
		AbstractMiningTask dTask = com.readValue(json, AbstractMiningTask.class);
		log.info("dTask: " + dTask);
	}
	
	@Test
	public void rankTest() throws Exception {
		CustomObjectMapper com = new CustomObjectMapper();
		
		Map<String, Variable> types = Maps.newHashMap();
		types.put("a", new ContinuousVariable());
		
		AbstractMiningTask task = new RankTask(types, "a");
		
		final String json = com.writeValueAsString(task);
		
		log.info("JSON: " + json);
		AbstractMiningTask dTask = com.readValue(json, AbstractMiningTask.class);
		log.info("dTask: " + dTask);
	}
	
	@Test
	public void varTest() throws Exception {
		CustomObjectMapper com = new CustomObjectMapper();
		
		Variable var = new ContinuousVariable();
		
		final String json = com.writeValueAsString(var);
		
		System.out.println("JSON: " + json);
		Variable dVar = com.readValue(json, Variable.class);
		System.out.println("dVar: " + dVar);
		
		System.out.println(dVar.getClass());
	}
}
