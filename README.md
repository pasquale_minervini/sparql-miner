sparql-miner:
-------------------

The sparql-miner service allows to execute Machine Learning (in particular, Clustering and Ranking) tasks on the results of SPARQL
queries, obtained from Linked Open Data (LOD) SPARQL endpoints.

To get the code:
-------------------
Clone the repository:

    $ git clone https://pasquale_minervini@bitbucket.org/pasquale_minervini/sparql-miner.git

If this is your first time using Bitbucket, review https://bitbucket.org/support to learn the basics.

To run the application:
-------------------	
From the command line with Maven:

    $ cd sparql-miner
    $ mvn tomcat7:run [-Dmaven.tomcat.port=<port no.>] (In case 9966 is busy] 

or

In your preferred IDE such as SpringSource Tool Suite (STS) or IDEA:

* Import sparql-miner as a Maven Project
* Drag-n-drop the project onto the "SpringSource tc Server Developer Edition" or another Servlet 2.5 or > Server to run, such as Tomcat.

Access the REST Web Service at: http://localhost:9966/sm/

Note:
-------------------

The REST APIs implemented by the sparql-miner service are described in the API/api.pdf document, while the architecture of this project is
described in architecture/project.pdf. The sparql-miner service has been developed in the context of the Puglia@Service project.
