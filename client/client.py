#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests

import os, sys
import logging

__author__ = 'pasquale'
__copyright__ = 'Università degli Studi di Bari Aldo Moro'
__service_name__ = 'sparql-miner'

def main(argv):
    logging.info('Querying the remote end-point through the %s service ..' % __service_name__)

    params = {
        'query': 'SELECT ?p ?o WHERE { <http://dbpedia.org/resource/Barack_Obama> ?p ?o . } LIMIT 10',
        'endpoint': 'http://dbpedia.org/sparql'
    }
    r = requests.get('http://127.0.0.1:9966/sm/v1/query/sparql', params=params)

    logging.info(r.status_code)
    logging.info(r.json())

    


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    main(sys.argv[1:])
