% !TeX spellcheck = it_IT

\documentclass{article}

\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref}

\usepackage{graphicx}

\usepackage{caption}
\usepackage{subcaption}

\usepackage[utf8x]{inputenc}

\usepackage{tikz}
\usetikzlibrary{bayesnet,snakes,arrows,shapes}

%\usepackage{tkz-berge}

\usepackage{amsmath,amsfonts,amssymb}
\usepackage{verbatim}
\usepackage{algorithm,algorithmic}

\usepackage{listings}
\usepackage{mathtools}
\usepackage{cite}

\usepackage{paralist}
\usepackage{fancybox}

\newtheorem{proposition}{Proposition}[section]
\newtheorem{corollary}[proposition]{Corollary}
\newtheorem{lemma}[proposition]{Lemma}
\newtheorem{theorem}[proposition]{Theorem}
\newtheorem{definition}{Definition}[section]
\newtheorem{ass}{Assumption}[section]
\newtheorem{example}{Example}[section]

\begin{document}

\title{{\sc SPARQL-Miner} \\ \vspace{50px} \Large Un servizio per l'interrogazione e il mining su basi di conoscenza Linked Open Data}

\maketitle

\section{Introduzione}

In questo documento è descritto  {\sc SPARQL-Miner}, un servizio per:
%
\begin{itemize}
%
 \item l'\emph{interrogazione} di basi di conoscenza Linked Open Data~\cite{Heath2011} (LOD), attraverso il corrispondente endpoint SPARQL~\cite{Prud'hommeaux:08:SQL};
%
 \item l'esecuzione di task di \emph{mining}~\cite{Han:2005:DMC:1076797} (clustering, ranking) sul risultato di tali interrogazioni.
%
\end{itemize}
%

%
Ogni funzionalità è implementata attraverso delle API REST~\cite{Fielding:2002:PDM:514183.514185}, in cui i parametri sono descritti o tramite stringhe, o tramite documenti JSON~\cite{rfc7159}.
%
In questo documento, sono riportate le specifiche delle API esposte da {\sc SPARQL-Miner} e dei rispettivi parametri.
%

Il resto di questo documento è organizzato come segue: in Sez.~\ref{sec:interrogazione} sono descritte le funzionalità di interrogazione; in Sec.~\ref{sec:clustering} sono descritte le funzionalità di clustering; in Sez.~\ref{sec:ranking} sono descritte le funzionalità di ranking; in Sez.~\ref{sec:both} sono descritte delle funzionalità che estendono i risultati di ogni interrogazione con risultati derivanti da task di mining non supervisionato.

\clearpage

\section{Interrogazione} \label{sec:interrogazione}

Questo modulo implementa le funzionalità di \emph{interrogazione} di {\sc SPARQL-Miner}.
%
Data una base di conoscenza LOD, l'obiettivo di questa componente è di effettuare interrogazioni, in forma di query SPARQL, su tale base di conoscenza, utilizzando il corrispondente endpoint.

\subsection{Tipi}

\begin{description}
 \item[Query Result]: descrive un insieme di \emph{variable binding}, i.e.\ di coppie, dove a ogni nome di una variabile (stringa), è associato il valore corrispondente (tipo primitivo).
 %
 Ogni Query Result è codificato da un documento JSON strutturato come segue:
  \begin{verbatim}
{ 
   variableName1: binding1,
   ..,
   variableNameN: bindingN
}
  \end{verbatim}
\end{description}

\subsection{API}

\begin{itemize}
%
 \item {\tt http://example.com/v1/query/sparql?query=\ovalbox{\$query}\&endpoint=\ovalbox{\$endpoint}}
 \begin{description}
  \item[Metodo di Richiesta]: {\bf GET}
  \item[Parametri]:
   \begin{description}
    \item[\$query]: query SPARQL (stringa).
    \item[\$endpoint]: endpoint SPARQL (stringa).
   \end{description}
  \item[Risultato]: documento JSON costituito da una lista di Query Result:
   \begin{verbatim}
[
   result1,
   ..,
   resultN
]
   \end{verbatim} 
 \end{description}
%
\end{itemize}

\clearpage

\section{Clustering} \label{sec:clustering}

In questa sezione vengono descritte le funzionalità di clustering di {\sc SPARQL-Miner}.

\subsection{Tipi}

\begin{description}
 \item[Variable]: descrive una variabile (covariata) tipizzata (discreta o continua).
 %
 Ogni Variable è codificata da un documento JSON strutturato come segue:
  \begin{verbatim}
{
   type: 'continuous' or 'discrete',
   values: [ value1, .., valueN ] (optional)
}
  \end{verbatim}
 \item[Data Set]: descrive un Data Set, composto da un insieme di Query Result.
 %
 Ogni Data Set è codificato da un documento JSON strutturato come segue:
  \begin{verbatim}
{
   values: [
      queryResult1,
      ..,
      queryResultN
   ]
}
  \end{verbatim}
 \item[Clustering Task]: descrive un task di clustering, dove vengono specificati i tipi delle variabili coinvolte nel task di mining.
 %
 Ogni Clustering Task è codificato da un documento JSON strutturato come segue:
  \begin{verbatim}
{
   types: {
      variableName1: variable1,
      ..,
      variableNameN: variableN
   } (optional)
}
  \end{verbatim}
  Ogni oggetto \texttt{variable} è di tipo Variable.
\end{description}

\subsection{API}

\begin{itemize}
%
 \item {\tt http://example.com/v1/cluster/cluster} %?task=\ovalbox{\$task}\&dataset=\ovalbox{\$dataset}}
 \begin{description}
  \item[Metodo di Richiesta]: {\bf POST}
  \item[Parametri]: documento JSON strutturato come segue:
   \begin{verbatim}
{
   task: $task,
   dataSet: $dataSet
}
   \end{verbatim}
   \begin{description}
    \item[\$task]: Clustering Task.
    \item[\$dataSet]: Data Set.
   \end{description}
  \item[Risultato]: documento JSON costituito da una lista di coppie, in cui a ogni Query Result nel Data Set è associato il Cluster Id corrispondente (intero).
   \begin{verbatim}
[
   {
      queryResult: query_result_1,
      clusterId: cluster_id_1
   },
   ..,
   {
      queryResult: query_result_N,
      clusterId: cluster_id_N
   }
]
   \end{verbatim} 
 \end{description}
%
\end{itemize}

\clearpage

\section{Ranking} \label{sec:ranking}

In questa sezione vengono descritte le funzionalità di ranking di {\sc SPARQL-Miner}.

\subsection{Tipi}

\begin{description}
 \item[Ranking Task]: descrive un task di ranking, dove vengono specificati i tipi delle variabili coinvolte nel task di mining e la variabile dipendente.
 %
 Ogni Ranking Task è codificato da un documento JSON strutturato come segue:
  \begin{verbatim}
{
   types: {
      variableName1: variable1,
      ..,
      variableNameN: variableN
   } (optional),
   classVariable: 'name of the dependent variable'
}
  \end{verbatim}
\end{description}

\subsection{API}

\begin{itemize}
%
 \item {\tt http://example.com/v1/cluster/cluster} %?task=\ovalbox{\$task}\&dataset=\ovalbox{\$dataset}}
 \begin{description}
  \item[Metodo di Richiesta]: {\bf POST}
  \item[Parametri]: documento JSON strutturato come segue:
   \begin{verbatim}
{
   task: $task,
   dataSet: $dataSet
}
   \end{verbatim}
   \begin{description}
    \item[\$task]: Ranking Task.
    \item[\$dataset]: Data Set.
   \end{description}
  \item[Risultato]: documento JSON costituito da una lista di coppie, in cui a ogni Query Result nel Data Set è associato un valore di ranking (scalare).
   \begin{verbatim}
[
   {
      queryResult: query_result_1,
      rankingValue: ranking_value_1
   },
   ..,
   {
      queryResult: query_result_N,
      rankingValue: ranking_value_N
   }
]
   \end{verbatim} 
 \end{description}
%
\end{itemize}

\clearpage

\section{Interrogazione \& Mining} \label{sec:both}

In questa sezione viene descritta una componente di {\sc SPARQL-Miner} che estende le funzionalità di interrogazione con funzionalità di mining non supervisionato.

\subsection{API}

\begin{itemize}
%
 \item {\tt http://example.com/v1/sm/cluster} %?task=\ovalbox{\$task}\&query=\ovalbox{\$query}\&endpoint=\ovalbox{\$endpoint}}
 \begin{description}
  \item[Metodo di Richiesta]: {\bf GET}
  \item[Parametri]: documento JSON strutturato come segue:
     \begin{verbatim}
{
   task: $task,
   query: $query,
   endpoint: $endpoint
}
     \end{verbatim}
   \begin{description}
    \item[\$task]: Clustering Task.
    \item[\$query]: query SPARQL (stringa).
    \item[\$endpoint]: endpoint SPARQL (stringa).
   \end{description}
  \item[Risultato]: documento JSON costituito da una lista di coppie, a cui ad ogni Query Result è associato il corrispondente cluster id..
   \begin{verbatim}
[
   {
      queryResult: query_result_1,
      clusterId: cluster_id_1
   },
   ..,
   {
      queryResult: query_result_N,
      clusterId: cluster_id_N
   }
]
   \end{verbatim} 
 \end{description}
%
\end{itemize}

\clearpage

\bibliographystyle{apalike}
\bibliography{bibliography}

\end{document}
